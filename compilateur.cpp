//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2019 Pierre Jourlin
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur"


#include <string>
#include <iostream>
#include <cstdlib>
#include <set>
#include <map>
#include <FlexLexer.h>
#include "tokeniser.h"
#include <cstring>

#include <vector>
#include <algorithm>

using namespace std;

enum OPREL {EQU, DIFF, INF, SUP, INFE, SUPE, WTFR};
enum OPADD {ADD, SUB, OR, WTFA};
enum OPMUL {MUL, DIV, MOD, AND ,WTFM};
enum TYPE {BOOLEAN,INTEGER};

vector<string> vecKeyw {"IF","THEN","ELSE","FOR","TO","DOWNTO","DO","WHILE"};
vector<string> vecTypes {"BOOLEAN","INTEGER"};

TOKEN current;				// Current token


FlexLexer* lexer = new yyFlexLexer; // This is the flex tokeniser
// tokens can be read using lexer->yylex()
// lexer->yylex() returns the type of the lexicon entry (see enum TOKEN in tokeniser.h)
// and lexer->YYText() returns the lexicon entry as a string

	
map<string,TYPE> DeclaredVariables;
unsigned long long TagNumber=0;

bool IsDeclared(const char *id){
	return DeclaredVariables.find(id)!=DeclaredVariables.end();
}


void Error(string s){
	cerr << "Ligne n°"<<lexer->lineno()<<", lu : '"<<lexer->YYText()<<"'("<<current<<"), mais ";
	cerr<< s << endl;
	exit(-1);
}

// Program := [DeclarationPart] StatementPart
// DeclarationPart := "[" Letter {"," Letter} "]"
// StatementPart := Statement {";" Statement} "."
// Statement := AssignementStatement | IfStatement | WhileStatement | 
//				ForStatement | BlockStatement
// AssignementStatement := Letter "=" Expression
// IfStatement := "IF" Expression "THEN" Statement ["ELSE" Statement]
// WhileStatement := "WHILE" Expression "DO" Statement
// ForStatement := "FOR" AssignementStatement "TO" Expression "DO" Statement
// BlockStatement := "BEGIN" Statement {";" Statement} "END"


// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := Digit{Digit}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"
	
		
TYPE Identifier(void){
	TYPE type;
	if(!IsDeclared(lexer->YYText())){
		cerr<<"Erreur : Variable'"<<lexer->YYText()<<"' non declaree"<<endl;
		exit(-1);
	}
	type=DeclaredVariables[lexer->YYText()];
	cout << "\tpush "<<lexer->YYText()<<endl;
	current=(TOKEN) lexer->yylex();
	return type;
}

TYPE Number(void){
	cout <<"\tpush $"<<atoi(lexer->YYText())<<endl;
	current=(TOKEN) lexer->yylex();
	return TYPE::INTEGER;//
}

TYPE Expression(void);			// Called by Term() and calls Term()

TYPE Factor(void){
	TYPE type;
	if(current==RPARENT){
		current=(TOKEN) lexer->yylex();
		type=Expression();//
		if(current!=LPARENT)
			Error("')' était attendu");		// ")" expected
		else
			current=(TOKEN) lexer->yylex();
	}
	else 
		if (current==NUMBER)
			type=Number();
	     	else
				if(current==ID)
					type=Identifier();
				else
					Error("'(' ou chiffre ou lettre attendue");
	return type;
}

// MultiplicativeOperator := "*" | "/" | "%" | "&&"
OPMUL MultiplicativeOperator(void){
	OPMUL opmul;
	if(strcmp(lexer->YYText(),"*")==0)
		opmul=MUL;
	else if(strcmp(lexer->YYText(),"/")==0)
		opmul=DIV;
	else if(strcmp(lexer->YYText(),"%")==0)
		opmul=MOD;
	else if(strcmp(lexer->YYText(),"&&")==0)
		opmul=AND;
	else opmul=WTFM;
	current=(TOKEN) lexer->yylex();
	return opmul;
}

// Term := Factor {MultiplicativeOperator Factor}
TYPE Term(void){
	TYPE type1;
	TYPE type2;
	OPMUL mulop;
	type1=Factor();//
	while(current==MULOP){
		mulop=MultiplicativeOperator();		// Save operator in local variable
		type2=Factor();
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		switch(mulop){
			case AND:
				if(type2!=TYPE::BOOLEAN)
					Error("le type doit etre BOOLEAN");
				cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
				cout << "\tpush %rax\t# AND"<<endl;	// store result
				break;
			case MUL:
				if(type2!=TYPE::INTEGER)
					Error("Le type doit etre INTEGER");
				cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
				cout << "\tpush %rax\t# MUL"<<endl;	// store result
				break;
			case DIV:
				if(type2!=TYPE::INTEGER)
					Error("le type doit etre INTEGER");
				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
				cout << "\tdiv %rbx"<<endl;			// quotient goes to %rax
				cout << "\tpush %rax\t# DIV"<<endl;		// store result
				break;
			case MOD:
				if(type2!=TYPE::INTEGER)
					Error("le type doit etre INTEGER");
				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
				cout << "\tdiv %rbx"<<endl;			// remainder goes to %rdx
				cout << "\tpush %rdx\t# MOD"<<endl;		// store result
				break;
			default:
				Error("opérateur multiplicatif attendu");
		}
	}
	return type1;
}

// AdditiveOperator := "+" | "-" | "||"
OPADD AdditiveOperator(void){
	OPADD opadd;
	if(strcmp(lexer->YYText(),"+")==0)
		opadd=ADD;
	else if(strcmp(lexer->YYText(),"-")==0)
		opadd=SUB;
	else if(strcmp(lexer->YYText(),"||")==0)
		opadd=OR;
	else opadd=WTFA;
	current=(TOKEN) lexer->yylex();
	return opadd;
}

// SimpleExpression := Term {AdditiveOperator Term}
TYPE SimpleExpression(void){
	TYPE type1;
	TYPE type2;
	OPADD adop;
	type1=Term();
	while(current==ADDOP){
		adop=AdditiveOperator();		// Save operator in local variable
		type2=Term();
		if(type2!=type1)
			Error("les types ne sont pas les memes");
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		switch(adop){
			case OR:
				if(type2!=TYPE::BOOLEAN)
					Error("le type doit etre BOOLEAN");
				cout << "\taddq	%rbx, %rax\t# OR"<<endl;// operand1 OR operand2
				break;			
			case ADD:
				if(type2!=TYPE::INTEGER)
					Error("le type doit etre INTEGER");
				cout << "\taddq	%rbx, %rax\t# ADD"<<endl;	// add both operands
				break;			
			case SUB:
				if(type2!=TYPE::INTEGER)
					Error("le type doit etre INTEGER");
				cout << "\tsubq	%rbx, %rax\t# SUB"<<endl;	// substract both operands
				break;
			default:
				Error("opérateur additif inconnu");
		}
		cout << "\tpush %rax"<<endl;			// store result
	}
	return type1;
}

//Type := "BOOLEAN" | "INTEGER"
TYPE Type(){
	if(current!=KEYWORD)
		Error("type attendu");
	if(strcmp(lexer->YYText(),"BOOLEAN")==0){
		current=(TOKEN)lexer->yylex();
		return TYPE::BOOLEAN;
	}
	else if(strcmp(lexer->YYText(),"INTEGER")==0){
		current=(TOKEN)lexer->yylex();
		return TYPE::INTEGER;
	}
	else
		Error("type inconnu");
	
	/*TYPE t;
	if(find(vecTypes.begin(),vecTypes.end(),lexer->YYText())==vecTypes.end())
		Error("Le type "+(string)lexer->YYText()+" n'est pas encore implémenté");
	else if(strcmp(lexer->YYText(),"BOOLEAN")==1)
		t=BOOLEAN;
	else   //if(strcmp(lexer->YYText(),"INTEGER")==1)
		t=INTEGER;
	return t;*/
}

// Declaration := Ident {"," Ident} ":" Type
void Declaration(){
	
	set<string> ids;
	TYPE type;
	if(current!=ID)
		Error("id attendu");
	ids.insert(lexer->YYText());
	current=(TOKEN)lexer->yylex();
	while(current==COMMA){
		current=(TOKEN)lexer->yylex();
		if(current!=ID)
			Error("id attendu");
		ids.insert(lexer->YYText());
		current=(TOKEN)lexer->yylex();
	}
	if(current!=COLON)
		Error("']' attendu");
	current=(TOKEN)lexer->yylex();
	type=Type();
	for(set<string>::iterator it=ids.begin();it!=ids.end();++it){
		cout << *it << ":\t.quad 0"<<endl;
		DeclaredVariables[*it]=type;
	}
	
	
	/*if(current!=RBRACKET)//
		Error("'[' attendu");
	current=(TOKEN) lexer->yylex();
	if(current!=ID)
		Error("id attendu");
	cout << lexer->YYText() << ":\t.quad 0"<<endl;
	DeclaredVariables[lexer->YYText()]=Type::INTEGER;
	current=(TOKEN) lexer->yylex();
	while(current==COMMA){
		current=(TOKEN) lexer->yylex();
		if(current!=ID)
			Error("id attendu");
		cout << lexer->YYText() << ":\t.quad 0"<<endl;
		DeclaredVariables[lexer->YYText()]=Type::INTEGER;
		current=(TOKEN) lexer->yylex();
	}
	if(current!=LBRACKET)//
		Error("caractere ']' attendu");
	current=(TOKEN) lexer->yylex();*/
}

// DeclarationPart := "VAR" Declaration {";" Declaration} "."
void DeclarationPart(void){
	current=(TOKEN)lexer->yylex();
	Declaration();
	while(current==SEMICOLON){
		current=(TOKEN)lexer->yylex();
		Declaration();
	}
	if(current!=DOT)
		Error("'.' attendu");
	current=(TOKEN)lexer->yylex();
	/*if(current!=RBRACKET)
		Error("caractère '[' attendu");
	cout << "\t.data"<<endl;
	cout << "\t.align 8"<<endl;
	
	current=(TOKEN) lexer->yylex();
	if(current!=ID)
		Error("Un identificater était attendu");
	cout << lexer->YYText() << ":\t.quad 0"<<endl;
	DeclaredVariables[lexer->YYText()]=Type::INTEGER;
	current=(TOKEN) lexer->yylex();
	while(current==COMMA){
		current=(TOKEN) lexer->yylex();
		if(current!=ID)
			Error("Un identificateur était attendu");
		cout << lexer->YYText() << ":\t.quad 0"<<endl;
		DeclaredVariables[lexer->YYText()]=Type::INTEGER;
		current=(TOKEN) lexer->yylex();
	}
	if(current!=LBRACKET)
		Error("caractère ']' attendu");
	current=(TOKEN) lexer->yylex();*/
}

// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
OPREL RelationalOperator(void){
	OPREL oprel;
	if(strcmp(lexer->YYText(),"==")==0)
		oprel=EQU;
	else if(strcmp(lexer->YYText(),"!=")==0)
		oprel=DIFF;
	else if(strcmp(lexer->YYText(),"<")==0)
		oprel=INF;
	else if(strcmp(lexer->YYText(),">")==0)
		oprel=SUP;
	else if(strcmp(lexer->YYText(),"<=")==0)
		oprel=INFE;
	else if(strcmp(lexer->YYText(),">=")==0)
		oprel=SUPE;
	else oprel=WTFR;
	current=(TOKEN) lexer->yylex();
	return oprel;
}

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
TYPE Expression(void){
	TYPE type1;
	TYPE type2;
	unsigned long long tag;
	OPREL oprel;
	type1=SimpleExpression();
	if(current==RELOP){
		tag=++TagNumber;
		oprel=RelationalOperator();
		type2=SimpleExpression();
		if(type2!=type1)
			Error("les types ne sont pas les memes");
		cout << "\tpop %rax"<<endl;
		cout << "\tpop %rbx"<<endl;
		cout << "\tcmpq %rax, %rbx"<<endl;
		switch(oprel){
			case EQU:
				cout << "\tje Vrai"<<tag<<"\t# If equal"<<endl;
				break;
			case DIFF:
				cout << "\tjne Vrai"<<tag<<"\t# If different"<<endl;
				break;
			case SUPE:
				cout << "\tjae Vrai"<<tag<<"\t# If above or equal"<<endl;
				break;
			case INFE:
				cout << "\tjbe Vrai"<<tag<<"\t# If below or equal"<<endl;
				break;
			case INF:
				cout << "\tjb Vrai"<<tag<<"\t# If below"<<endl;
				break;
			case SUP:
				cout << "\tja Vrai"<<tag<<"\t# If above"<<endl;
				break;
			default:
				Error("Opérateur de comparaison inconnu");
		}
		cout << "\tpush $0\t\t# False"<<endl;
		cout << "\tjmp Suite"<<tag<<endl;
		cout << "Vrai"<<tag<<":\tpush $0xFFFFFFFFFFFFFFFF\t\t# True"<<endl;	
		cout << "Suite"<<tag<<":"<<endl;
		return TYPE::BOOLEAN;
	}
	return type1;
}

// AssignementStatement := Identifier ":=" Expression
void AssignementStatement(void){
	TYPE type1;
	TYPE type2;
	std::string variable;
	if(current!=ID)
		Error("Identificateur attendu");
	if(!IsDeclared(lexer->YYText())){
		cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	variable=lexer->YYText();
	type1=DeclaredVariables[variable];
	current=(TOKEN) lexer->yylex();
	if(current!=ASSIGN)
		Error("caractères ':=' attendus");
	current=(TOKEN) lexer->yylex();
	type2=Expression();
	if(type2!=type1)
		Error("les types ne sont pas les memes");
	cout << "\tpop "<<variable<<endl;
}

// DisplayStatement := "DISPLAY" Expression
void DisplayStatement(void){
	TYPE type;
	unsigned long long tag=++TagNumber;
	
	current=(TOKEN) lexer->yylex();
	type=Expression();
	if(type==TYPE::INTEGER){
		cout << "\tpop %rdx\t# The value to be displayed"<<endl;
		cout << "\tmovq $FormatString1, %rsi\t# \"%llu\\n\""<<endl;
	}
	else if(type==TYPE::BOOLEAN){
			cout << "\tpop %rdx\t# Zero : False, non-zero : true"<<endl;
			cout << "\tcmpq $0, %rdx"<<endl;
			cout << "\tje False"<<tag<<endl;
			cout << "\tmovq $TrueString, %rsi\t# \"TRUE\\n\""<<endl;
			cout << "\tjmp Next"<<tag<<endl;
			cout << "False"<<tag<<":"<<endl;
			cout << "\tmovq $FalseString, %rsi\t# \"FALSE\\n\""<<endl;
			cout << "Next"<<tag<<":"<<endl;
	}
	else
			Error("DISPLAY ne fonctionne que pour les nombres entiers");
	cout << "\tmovl	$1, %edi"<<endl;
	cout << "\tmovl	$0, %eax"<<endl;
	cout << "\tcall	__printf_chk@PLT"<<endl;
}

void Statement(void);

// IfStatement := "IF" Expression "THEN" Statement ["ELSE" Statement]
void IfStatement(void){
	unsigned long long tag=TagNumber++;
	current=(TOKEN)lexer->yylex();
	if(Expression()!=TYPE::BOOLEAN)
		Error("type booleen attendu");
	cout<<"\tpop %rax\t# Get the result of expression"<<endl;
	cout<<"\tcmpq $0, %rax"<<endl;
	cout<<"\tje Else"<<tag<<"\t# if FALSE, jump to Else"<<tag<<endl;
	if(current!=KEYWORD||strcmp(lexer->YYText(),"THEN")!=0)
		Error("mot cle THEN attendu");
	current=(TOKEN) lexer->yylex();
	Statement();
	cout<<"\tjmp Next"<<tag<<"\t# Do not execute the else statement"<<endl;
	cout<<"Else"<<tag<<":"<<endl; // Might be the same effective adress than Next:
	if(current==KEYWORD&&strcmp(lexer->YYText(),"ELSE")==0){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	cout<<"Next"<<tag<<":"<<endl;

	/*if(strcmp(lexer->YYText(),"IF")==0)
		Error("IF attendu");
		
	Expression();

	if(strcmp(lexer->YYText(),"THEN")==0)//
		Error("THEN attendu");//

	if(strcmp(lexer->YYText(),"ELSE")==1)//
		Statement();//*/
}

// WhileStatement := "WHILE" Expression "DO" Statement
void WhileStatement(void){
	unsigned long long tag=TagNumber++;
	cout<<"While"<<tag<<":"<<endl;
	current=(TOKEN) lexer->yylex();
	if(Expression()!=TYPE::BOOLEAN)
		Error("type booleen attendu");
	cout<<"\tpop %rax\t# Get the result of expression"<<endl;
	cout<<"\tcmpq $0, %rax"<<endl;
	cout<<"\tje EndWhile"<<tag<<"\t# if FALSE, jump out of the loop"<<tag<<endl;
	if(current!=KEYWORD||strcmp(lexer->YYText(),"DO")!=0)
		Error("mot cle DO attendu");
	current=(TOKEN) lexer->yylex();
	Statement();
	cout<<"\tjmp While"<<tag<<endl;
	cout<<"EndWhile"<<tag<<":"<<endl;
	
	/*if(strcmp(lexer->YYText(),"WHILE")==0)
		Error("WHILE attendu");
	//current=lexer->yylex()
	Expression();
	
	//if(current!=KEYWORD||strcmp(lexer->yylex(),"DO")!=0)
	//Error("DO attendu");
	if(strcmp(lexer->YYText(),"DO")==0)//
		Error("DO attendu");//
	
	Statement();*/
}

// ForStatement := "For" ID ":=" Expression ("TO"|"DOWNTO") Expression "DO" Statement
void ForStatement(void){
	if(strcmp(lexer->YYText(),"FOR")==0)
		Error("FOR attendu");
	AssignementStatement();
	if(strcmp(lexer->YYText(),"TO")==0)
		Error("TO attendu");
	Expression();
	if(strcmp(lexer->YYText(),"DO")==0)
		Error("DO attendu");
	Statement();
}

// BlockStatement := "BEGIN" Statement {";" Statement} "END"
void BlockStatement(void){
	current=(TOKEN)lexer->yylex();
	Statement();
	while(current==SEMICOLON){
		current=(TOKEN)lexer->yylex();
		Statement();
	};
	if(current!=KEYWORD||strcmp(lexer->YYText(),"END")!=0)
		Error("mot cle END attendu");
	current=(TOKEN)lexer->yylex();
	
	
	/*if(strcmp(lexer->YYText(),"BEGIN")==0)
		Error("BEGIN attendu");
	Statement();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(strcmp(lexer->YYText(),"END")==0)
		Error("END attendu");*/
}

// Statement := AssignementStatement|DisplayStatement
void Statement(void){
	if(current==KEYWORD){
		if(find(vecKeyw.begin(),vecKeyw.end(),lexer->YYText())==vecKeyw.end()){
			cerr<<"Le mot clé "<<lexer->yylex()<<" n'est pas encore implémenté";
			Error(".");
		}
		else if(strcmp(lexer->YYText(),"DISPLAY")==0)
			DisplayStatement();
		else if(strcmp(lexer->YYText(),":=")==1){
			AssignementStatement();
		}
		else if(strcmp(lexer->YYText(),"IF")==1){
			IfStatement();
		}
		else if(strcmp(lexer->YYText(),"WHILE")==1){
			WhileStatement();
		}
		else if(strcmp(lexer->YYText(),"FOR")==1){
			ForStatement();
		}
		else //if(strcmp(lexer->YYText(),"BEGIN")==0)
			BlockStatement();
	}
}


// StatementPart := Statement {";" Statement} "."
void StatementPart(void){
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;
	Statement();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(current!=DOT)
		Error("caractère '.' attendu");
	current=(TOKEN) lexer->yylex();
}

// Program := [DeclarationPart] StatementPart
void Program(void){
	if(current==KEYWORD&&strcmp(lexer->YYText(),"VAR")==0)
		DeclarationPart();
	StatementPart();	
}


int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;
	cout << ".data"<<endl;
	cout << "FormatString1:\t.string \"%llu\\n\"\t# used by printf to display 64-bit unsigned integers"<<endl; 
	cout << "TrueString:\t.string \"TRUE\\n\"\t# used by printf to display the boolean value TRUE"<<endl; 
	cout << "FalseString:\t.string \"FALSE\\n\"\t# used by printf to display the boolean value FALSE"<<endl; 
	// Let's proceed to the analysis and code production
	current=(TOKEN) lexer->yylex();
	Program();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(current!=FEOF){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}
}
