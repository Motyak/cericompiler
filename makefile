all:	test
clean:
		rm *.o *.s
		rm tokeniser.cpp
tokeniser.cpp:	tokeniser.l
		flex++ -d -otokeniser.cpp tokeniser.l
tokeniser.o:	tokeniser.cpp
		g++ -c tokeniser.cpp
compilateur:	compilateur.cpp tokeniser.o
		g++ -std=c++11 -ggdb -o compilateur compilateur.cpp tokeniser.o
test.s:		compilateur test2.p
		./compilateur <test2.p >test2.s
test:		test2.s
		gcc -ggdb test2.s -o test


